//
//  DWFibonacciTableViewController.h
//  DWFibonacci
//
//  Created by Doug Watkins on 3/24/15.
//  Copyright (c) 2015 Doug Watkins. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DWFibonacciTableViewController : UITableViewController {
}

@property (nonatomic, strong) NSMutableArray *fib;

@end

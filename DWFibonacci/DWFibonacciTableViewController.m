//
//  DWFibonacciTableViewController.m
//  DWFibonacci
//
//  Created by Doug Watkins on 3/24/15.
//  Copyright (c) 2015 Doug Watkins. All rights reserved.
//

#import "DWFibonacciTableViewController.h"

@interface DWFibonacciTableViewController ()

@end

@implementation DWFibonacciTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
	unsigned int nextFib = 1;
	unsigned int lastFib = 0;
	self.fib = [[NSMutableArray alloc] init];
	[self.fib addObject:[NSNumber numberWithUnsignedInt:0]];
	[self.fib addObject:[NSNumber numberWithUnsignedInt:1]];
	do {
		unsigned int currentFib = lastFib;
		lastFib = nextFib;
		nextFib = currentFib + lastFib;
		if (nextFib >= lastFib) {
//			NSLog(@"Current %u, last %u, next %u", currentFib, lastFib, nextFib);
			[self.fib addObject:[NSNumber numberWithUnsignedInt:nextFib]];
		}
	} while (nextFib >= lastFib);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return self.fib.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    // Configure the cell...
	NSNumber *number = [self.fib objectAtIndex:[indexPath row]];
	cell.textLabel.text = [NSString stringWithFormat:@"%@", number];
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
